This is the official [Ruby](https://www.ruby-lang.org) client for [Dr. Ashtetepe](https://drhttp.com/) service.

*Note: At the moment, as we are developing Dr. Ashtetepe, we ask you to use [Moesif](https://www.moesif.com/)'s [Ruby library](https://github.com/Moesif/moesif-rack). Thanks to them to permit that via their [Apache licence](https://raw.githubusercontent.com/Moesif/moesif-rack/master/LICENSE)*. This README is a [TL;DR](https://en.wiktionary.org/wiki/tl;dr) for Dr.Ashtetepe usage.

# Installation
> [An integration example is provided here](https://bitbucket.org/drhttp/drhttp-ruby/src/master/example/)

1) Install lua packages with [RubyGems](https://rubygems.org/)
    ```
    gem install moesif_rack
    ```

2) Retrieve a `dsn` ([Data source name](https://en.wikipedia.org/wiki/Data_source_name)) which can be found in [your project](https://drhttp.com/projects). eg: `https://<my_project_api_key>@api.drhttp.com/`

3) Configure the middleware in your application file:

    ```ruby
    require 'moesif_rack'
    MoesifApi::Configuration.base_uri = 'https://api.drhttp.com/moesif'
    use MoesifRack::MoesifMiddleware, {
        'application_id' => '<my_project_api_key>'
    }
    ```

### User identification

See [identify-user in Moesif documentation](https://github.com/Moesif/moesif-rack#identify_user)

### Device identification

*Note: Device identification is not available yet in the ruby library*

## Outbound request recording

*Note: Outbound request recording is not available yet in the ruby library*

# Troubleshooting

Please [report any issue](https://bitbucket.org/drhttp/drhttp-ruby/issues/new) you encounter concerning documentation or implementation. This is very much appreciated. We'll upstream the improvements to Moesif.
