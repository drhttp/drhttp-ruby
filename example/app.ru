require 'uri'
project_key = URI(ENV["DRHTTP_DSN"]).user

require 'moesif_rack'
MoesifApi::Configuration.base_uri = 'https://api.drhttp.com/moesif'
use MoesifRack::MoesifMiddleware, {
    'application_id' => project_key
}

class App
    def call(env)
        [200, {"Content-Type" => "text/html"}, ["Hello World!"]]
    end 
end

run App.new